// The root URL for the RESTful services
var rootURL = "http://localhost:8080/ted/rest/ted";

var currentActivite;

//$('#btnSave').click(function() {
	//updateWine();
//});
$('#btnSearch').click(function() {
	pourcentageActiviteDone($('#searchKey').val());
	return false;
});

$('#activiteList a').live('click', function() {
	pourcentageActiviteDone($(this).data('identity'));
});

function partageActivite(id) {
	console.log('partageActivite ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/partageActivite/' + id,
		dataType: 'json',
		processData: false,
		success: function(data){
			//activites = data.activites;
			//result = xmlToJson(data);
			//console.log(result.activites.activite[0].nom);
			console.log(data);
			
			//console.log(data.activite[0])
			//currentActivite = activites;
			//renderDetails(currentActivite);
			//renderList(data.activite);
		}
	});
}

function endTask(id) {
	alert('endTask ' + id);
	console.log(id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/endTask/' + id,
		dataType: 'json',
		processData: false,
		success: function(data){
			//activites = data.activites;
			//result = xmlToJson(data);
			//console.log(result.activites.activite[0].nom);
			console.log(data);
			
			//console.log(data.activite[0])
			//currentActivite = activites;
			//renderDetails(currentActivite);
			//renderList(data.activite);
		}
	});
}

function pourcentageActiviteDone(id) {
	alert('pourcentageActiviteDone ' + id);
	console.log(id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/pourcentageActiviteDone/' + id,
		dataType: 'json',
		processData: false,
		success: function(data){
			console.log(data);
		}
	});
}

function findAllByCollaborateur(id) {
	alert('findAllByCollaborateur: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/findAllByCollaborateur/' + id,
		//dataType: 'xml',
		dataType: 'json',
		processData: false,
		success: function(data){
			//activites = data.activites;
			//result = xmlToJson(data);
			//console.log(result.activites.activite[0].nom);
			console.log(data);
			console.log(data.activite[0])
			//currentActivite = activites;
			//renderDetails(currentActivite);
			renderList(data.activite);
		}
	});
	alert('findAllByCollaborateur: ');
}

function findAllByCollaborateurPartage(id) {
	alert('findAllByCollaborateurPartage: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/findAllByCollaborateurPartage/' + id,
		//dataType: 'xml',
		dataType: 'json',
		processData: false,
		success: function(data){
			//activites = data.activites;
			//result = xmlToJson(data);
			//console.log(result.activites.activite[0].nom);
			console.log(data);
			console.log(data.activite[0])
			//currentActivite = activites;
			//renderDetails(currentActivite);
			renderList(data.activite);
		}
	});
	alert('findAllByCollaborateurPartage: ');
}
function dump(obj) {
	var out = '';
	for (var i in obj) {
		out += i + ": " + obj[i] + "\n";
	}
	console.log(out);
}

function updateWine() {
	console.log('updateWine');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#wineId').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Wine updated successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updateWine error: ' + textStatus);
		}
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);
	console.log("liste :" + list);
	$('#activiteList li').remove();
	$.each(list, function(index, activite) {
		console.log("activite = " + activite);
		console.log(activite.id);
		$('#activiteList').append('<li><a href="#" data-identity="' + activite.id + '">'+activite.nom+'</a></li>');
	});
}

function renderDetails(activite) {
	console.log(activite.id);
	console.log(activite.nom);
	$('#activiteid').val(activite.id);
	$('#activitenom').val(activite.nom);
	$('#activitedescription').val(activite.description);
	$('#activitedateCreation').val(activite.dateCreation);
	$('#activitedateEcheance').val(activite.dateEcheance);
	$('#activitetypeActivite').val(activite.typeActivite);
	$('#activitelibelleTypeActivite').val(activite.libelleTypeActivite);
	$('#activiteetat').val(activite.etat);
	$('#activitelibelleEtat').val(activite.libelleEtat);
	$('#activiteisPartage').val(activite.isPartage);
	$('#activiteisRecurrent').val(activite.isRecurrent);
	$('#activitelisteTaches').val(activite.listeTaches);
	$('#activiteisMaitre').val(activite.isMaitre);
	$('#activiteisInProgress').val(activite.isInProgress);
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var activiteId = $('#activiteId').val();
	return JSON.stringify({
		"id": activiteId == "" ? null : activiteId, 
		"nom": $('#nom').val(), 
		"description": $('#description').val(),
		"dateCreation": $('#dateCreation').val(),
		"dateEcheance": $('#dateEcheance').val(),
		"typeActivite": $('#typeActivite').val(),
		"libelleTypeActivite": $('#libelleTypeActivite').val(),
		"etat": $('#etat').val(),
		"libelleEtat": $('#libelleEtat').val(),
		"isPartage": $('#isPartage').val(),
		"isRecurrent": $('#isRecurrent').val(),
		"listeTaches": $('#listeTaches').val(),
		"isMaitre": $('#isMaitre').val(),
		"isInProgress": $('#isInProgress').val()
		});
}