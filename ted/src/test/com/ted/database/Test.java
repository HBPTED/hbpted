package test.com.ted.database;

import com.ted.database.object.Activite;

import java.util.List;

import com.ted.database.dao.concret.ActiviteDAO;

public class Test {
	public static void main(String[] args) {
		testUpdate();
    }
    
    public static void testFind(){
    	ActiviteDAO dao = new ActiviteDAO();
        List<Activite> list = dao.findAllActivitiesPartage();
        for(int i=0; i<list.size(); i++){
        	System.out.println(list.get(i).getNom());
        	System.out.println(list.get(i).isMaitre());
        	System.out.println(list.get(i).isPartage());
        	System.out.println(list.get(i).isInProgress());
        	
        }
        System.out.println(list.size());
    }
     
    public static void testUpdate(){
    	ActiviteDAO dao = new ActiviteDAO();
    	//boolean a = dao.endTask(1);
    	//boolean b = dao.endTask(2);
    	//boolean d = dao.partageActivite(4);
    	String c = dao.createActivite("Dossier test", "Test", "", 2);
    	System.out.println(c);
    }
}
