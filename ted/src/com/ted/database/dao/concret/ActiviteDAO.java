package com.ted.database.dao.concret;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ted.database.connection.ConnectionPostgreSQL;
import com.ted.database.object.Activite;
import com.ted.database.object.Tache;

public class ActiviteDAO{

	public Connection connect = ConnectionPostgreSQL.getInstance();
	
	public List<Activite> findAllByCollaborateur(int idCollaborateur) {
		List<Activite> listActivite = new ArrayList<>();
        try {
        	String requete = "SELECT * FROM activite a inner join collaborateuractivite b on b.idactivite = a.id " +
        			" where b.idcollaborateur =" + idCollaborateur +
        			" and a.etat <> 4" +
        			" and ((a.ispartage = false and b.ismaitre = true) " +
        			" or (a.ispartage = true and b.ismaitre = false));";
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(requete);
            while(result.next()) {
            	int id = result.getInt("id");
            	String nom = result.getString("nom");
            	String description = result.getString("description");
            	String dateCreation = result.getString("datecreation");
            	String dateEcheance = result.getString("dateecheance");
            	int typeActiviteid = result.getInt("typeactivite");
            	int etatid = result.getInt("etat");
            	boolean isPartage = result.getBoolean("ispartage");
            	boolean isRecurrent = result.getBoolean("isrecurrent");
            	boolean isMaitre = result.getBoolean("ismaitre");
            	String libelleEtat = getLibelleEtat(etatid);
            	String libelleTypeActivite = getLibelleTypeActivite(typeActiviteid);
            	List<Tache> listeTaches = getListeTaches(id);
            	listActivite.add(new Activite(id, nom, description, dateCreation, dateEcheance, typeActiviteid, libelleTypeActivite, etatid, libelleEtat, isPartage, isRecurrent, listeTaches, isMaitre, false));          
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listActivite;
    }
	
	public List<Activite> findAllByCollaborateurPartage(int idCollaborateur) {
		List<Activite> listActivite = new ArrayList<>();
        try {
        	String requete = "select *, " +
        		"case when exists (select x.idcollaborateur from collaborateuractivite x where x.idactivite = a.id and x.ismaitre = false) then true else false end as isinprogress" +
        		" from activite a inner join collaborateuractivite b on b.idactivite = a.id " + 
        		" where b.idcollaborateur = " + idCollaborateur +
        		" and a.etat <> 4"+
        		" and a.ispartage = true" +
        		" and b.ismaitre = true;" ;
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(requete);
            while(result.next()) {
            	int id = result.getInt("id");
            	String nom = result.getString("nom");
            	String description = result.getString("description");
            	String dateCreation = result.getString("datecreation");
            	String dateEcheance = result.getString("dateecheance");
            	int typeActiviteid = result.getInt("typeactivite");
            	int etatid = result.getInt("etat");
            	boolean isPartage = result.getBoolean("ispartage");
            	boolean isRecurrent = result.getBoolean("isrecurrent");
            	boolean isMaitre = result.getBoolean("ismaitre");
            	String libelleEtat = getLibelleEtat(etatid);
            	String libelleTypeActivite = getLibelleTypeActivite(typeActiviteid);
            	List<Tache> listeTaches = getListeTaches(id);
            	boolean isInProgress = result.getBoolean("isinprogress");
            	listActivite.add(new Activite(id, nom, description, dateCreation, dateEcheance, typeActiviteid, libelleTypeActivite, etatid, libelleEtat, isPartage, isRecurrent, listeTaches, isMaitre, isInProgress));          
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listActivite;
	}

	// Non utilise
	public List<Activite> findAllByCollaborateurRecycle(int idCollaborateur) {
		List<Activite> listActivite = new ArrayList<>();
        try {
        	String requete = "select * from activite a inner join collaborateuractivite b on b.idactivite = a.id" + 
				" where b.idcollaborateur = "+ idCollaborateur +
				" and a.etat = 4;" ;
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(requete);
            while(result.next()) {
            	int id = result.getInt("id");
            	String nom = result.getString("nom");
            	String description = result.getString("description");
            	String dateCreation = result.getString("datecreation");
            	String dateEcheance = result.getString("dateecheance");
            	int typeActiviteid = result.getInt("typeactivite");
            	int etatid = result.getInt("etat");
            	boolean isPartage = result.getBoolean("ispartage");
            	boolean isRecurrent = result.getBoolean("isrecurrent");
            	boolean isMaitre = result.getBoolean("ismaitre");
            	String libelleEtat = getLibelleEtat(etatid);
            	String libelleTypeActivite = getLibelleTypeActivite(typeActiviteid);
            	List<Tache> listeTaches = getListeTaches(id);
            	listActivite.add(new Activite(id, nom, description, dateCreation, dateEcheance, typeActiviteid, libelleTypeActivite, etatid, libelleEtat, isPartage, isRecurrent, listeTaches, isMaitre, false));          
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listActivite;
	}

	public List<Activite> findAllActivitiesPartage() {
		List<Activite> listActivite = new ArrayList<>();
        try {
        	String requete = "select * from activite a inner join collaborateuractivite b on b.idactivite = a.id" + 
				" where a.etat <> 4 " +
				" and a.etat <> 3 " +
				" and a.ispartage = true" +
				" and a.id not in (select x.idactivite from collaborateuractivite x where x.ismaitre = false and x.idactivite = a.id);" ;
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(requete);
            while(result.next()) {
            	int id = result.getInt("id");
            	String nom = result.getString("nom");
            	String description = result.getString("description");
            	String dateCreation = result.getString("datecreation");
            	String dateEcheance = result.getString("dateecheance");
            	int typeActiviteid = result.getInt("typeactivite");
            	int etatid = result.getInt("etat");
            	boolean isPartage = result.getBoolean("ispartage");
            	boolean isRecurrent = result.getBoolean("isrecurrent");
            	boolean isMaitre = result.getBoolean("ismaitre");
            	String libelleEtat = getLibelleEtat(etatid);
            	String libelleTypeActivite = getLibelleTypeActivite(typeActiviteid);
            	List<Tache> listeTaches = getListeTaches(id);
            	listActivite.add(new Activite(id, nom, description, dateCreation, dateEcheance, typeActiviteid, libelleTypeActivite, etatid, libelleEtat, isPartage, isRecurrent, listeTaches, isMaitre, false));          
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listActivite;
	}
	
	public boolean endActivity(int idActivite) {
		try {
            System.out.println("Start");
            this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate("UPDATE activite set etat = 3 where id =" + idActivite);
            System.out.println("End");
            return true;
        } catch(SQLException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	public boolean endTask (int idTache) {
		try {
            System.out.println("Start");
            this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate("UPDATE tache set istraite = true where id =" + idTache);
            System.out.println("End");
            return true;
        } catch(SQLException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	public boolean partageActivite (int idActivite) {
		try {
            System.out.println("Start");
            this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeUpdate("UPDATE activite set ispartage = true where id = " + idActivite);
            System.out.println("End");
            return true;
        } catch(SQLException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	public String getLibelleEtat(int etatid){
		try {
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM etat where id =" + etatid);
            if(result.first()) {
            	return result.getString("libelle");
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}  
		return "";
	}

	public String getLibelleTypeActivite(int typeActiviteid){
		try {
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM typeactivite where id =" + typeActiviteid);
            if(result.first()) {
            	return result.getString("libelle");
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}  
		return "";
	}
	
	public List<Tache> getListeTaches(int idActivite){
		List<Tache> listeTaches = new ArrayList<Tache>();
		try {
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM tache where idactivite =" + idActivite);
            while(result.next()) {
            	int id = result.getInt("id");
            	String nom = result.getString("nom");
            	boolean isTraite = result.getBoolean("istraite");
            	Tache t = new Tache(id, nom, idActivite, isTraite);
            	listeTaches.add(t);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}  
		return listeTaches;
	}
	
	public String pourcentageActiviteDone(int idCollaborateur) {
		try {
			String requete = "select sum(case when a.etat = 3 then 1 else 0 end) as sommeDone," +
					" sum(1) as sommeTotale," +
					" sum(ingredientcase when a.etat = 3 then 1 else 0 end)* 100.00 / sum(1) * 1.00 as pourcentage" +
					" from activite a inner join collaborateuractivite b on b.idactivite = a.id" +
					" where b.idcollaborateur = " +idCollaborateur +
					" and b.ismaitre = true" +
					" group by b.idcollaborateur;"	;
			
            ResultSet result  = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(requete);
            if(result.first()) {
            	double id = result.getDouble("pourcentage");
            	return "" + id ;
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}  
		return "";
	}
	
	public String createActivite(String nom, String description, String type, int idCollaborateur) {
		try {
            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT MAX(id) as max FROM activite");
            if(result.first()) {
                long id = result.getLong("max") + 1;
                PreparedStatement prepare = this.connect.prepareCall("INSERT INTO activite VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
                prepare.setString(1, description);
                prepare.setBoolean(2, false);
                prepare.setLong(3, id);
                prepare.setInt(4, 1);
                prepare.setInt(5, 1);
                prepare.setBoolean(6, false);
                prepare.setString(7, nom);
                prepare.setString(8, "15-12-2015");
                prepare.setString(9, "");
                prepare.executeUpdate();
                PreparedStatement prepare2 = this.connect.prepareCall("INSERT INTO collaborateuractivite VALUES (?, ?, ?)");
                prepare2.setLong(1, idCollaborateur);
                prepare2.setLong(2, id);
                prepare2.setBoolean(3, true);
                prepare2.executeUpdate();
                
                ResultSet result2 = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT MAX(id) as max FROM tache");
                if(result2.first()) {
                    long id2 = result.getLong("max") + 1;
                PreparedStatement prepare3 = this.connect.prepareCall("INSERT INTO tache VALUES (?, ?, ?, ?)");
                prepare3.setLong(1, id2);
                prepare3.setLong(2, id);
                prepare3.setBoolean(3, false);
                prepare3.setString(4, "Scanner documents");
                prepare3.executeUpdate();
                PreparedStatement prepare4 = this.connect.prepareCall("INSERT INTO tache VALUES (?, ?, ?, ?)");
                prepare4.setLong(1, id2 + 1);
                prepare4.setLong(2, id);
                prepare4.setBoolean(3, false);
                prepare4.setString(4, "Preparer contrat");
                prepare4.executeUpdate();
                }
                return "true";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "false";
	}
	
}
