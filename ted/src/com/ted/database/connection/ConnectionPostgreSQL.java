package com.ted.database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionPostgreSQL {

    // URL de connection
    private final static String url = "jdbc:postgresql://localhost:5432/ted";
    // Nom du user
    private final static String user = "tatiana";
    // Password
    private final static String password = "Password01";
    // Objet de connection
    private static Connection connect;
    
    public ConnectionPostgreSQL() {
    }

    public static Connection getInstance(){
    	try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found " + e);
        }
        if(connect == null) {
            try {
                connect = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connect;
    }
    
}