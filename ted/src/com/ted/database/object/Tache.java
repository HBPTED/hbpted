package com.ted.database.object;

public class Tache {
	private int id;
	private String nom;
	private int idActivite;
	private boolean isTraite;
	
	public Tache(int id, String nom, int idActivite, boolean isTraite) {
		super();
		this.id = id;
		this.nom = nom;
		this.idActivite = idActivite;
		this.isTraite = isTraite;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIdActivite() {
		return idActivite;
	}

	public void setIdActivite(int idActivite) {
		this.idActivite = idActivite;
	}

	public boolean isTraite() {
		return isTraite;
	}

	public void setTraite(boolean isTraite) {
		this.isTraite = isTraite;
	}
}
