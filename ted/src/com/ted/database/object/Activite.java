package com.ted.database.object;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Activite {

	private int id;
	private String nom;
	private String description;
	private String dateCreation;
	private String dateEcheance;
	private int typeActivite;
	private String libelleTypeActivite;
	private int etat;
	private String libelleEtat;
	private boolean isPartage;
	private boolean isRecurrent;
	private List<Tache> listeTaches;
	private boolean isMaitre;
	private boolean isInProgress;
	
	public Activite(){
		
	}
	
	public Activite(int id, String nom, String description, String dateCreation, String dateEcheance, int typeActivite,
			String libelleTypeActivite, int etat, String libelleEtat, boolean isPartage, boolean isRecurrent,
			List<Tache> listeTaches, boolean isMaitre, boolean isInProgress) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.dateCreation = dateCreation;
		this.dateEcheance = dateEcheance;
		this.typeActivite = typeActivite;
		this.libelleTypeActivite = libelleTypeActivite;
		this.etat = etat;
		this.libelleEtat = libelleEtat;
		this.isPartage = isPartage;
		this.isRecurrent = isRecurrent;
		this.listeTaches = listeTaches;
		this.isMaitre = isMaitre;
		this.isInProgress = isInProgress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getDateEcheance() {
		return dateEcheance;
	}

	public void setDateEcheance(String dateEcheance) {
		this.dateEcheance = dateEcheance;
	}

	public int getTypeActivite() {
		return typeActivite;
	}

	public void setTypeActivite(int typeActivite) {
		this.typeActivite = typeActivite;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public boolean isPartage() {
		return isPartage;
	}

	public void setPartage(boolean isPartage) {
		this.isPartage = isPartage;
	}

	public boolean isRecurrent() {
		return isRecurrent;
	}

	public void setRecurrent(boolean isRecurrent) {
		this.isRecurrent = isRecurrent;
	}

	public String getLibelleTypeActivite() {
		return libelleTypeActivite;
	}

	public void setLibelleTypeActivite(String libelleTypeActivite) {
		this.libelleTypeActivite = libelleTypeActivite;
	}

	public String getLibelleEtat() {
		return libelleEtat;
	}

	public void setLibelleEtat(String libelleEtat) {
		this.libelleEtat = libelleEtat;
	}

	public List<Tache> getListeTaches() {
		return listeTaches;
	}

	public void setListeTaches(List<Tache> listeTaches) {
		this.listeTaches = listeTaches;
	}

	public boolean isMaitre() {
		return isMaitre;
	}

	public void setMaitre(boolean isMaitre) {
		this.isMaitre = isMaitre;
	}

	public boolean isInProgress() {
		return isInProgress;
	}

	public void setInProgress(boolean isInProgress) {
		this.isInProgress = isInProgress;
	}
	
}
