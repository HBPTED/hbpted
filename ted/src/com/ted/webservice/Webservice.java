package com.ted.webservice;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ted.database.dao.concret.ActiviteDAO;
import com.ted.database.object.Activite;

@Path("/ted")
public class Webservice {

	ActiviteDAO dao = new ActiviteDAO();
	
	@GET @Path("/findAllByCollaborateur/{idCollaborateur}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public List<Activite> findAllByCollaborateur(@PathParam("idCollaborateur") int idCollaborateur) {
		System.out.println("findAllByCollaborateur");
		return dao.findAllByCollaborateur(idCollaborateur);
	}
	
	@GET @Path("/findAllByCollaborateurPartage/{idCollaborateur}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public List<Activite> findAllByCollaborateurPartage(@PathParam("idCollaborateur") int idCollaborateur) {
		System.out.println("findAllByCollaborateurPartage");
		return dao.findAllByCollaborateurPartage(idCollaborateur);
	}
	
	@GET @Path("/findAllActivitiesPartage/")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public List<Activite> findAllActivitiesPartage() {
		System.out.println("findAllActivitiesPartage");
		return dao.findAllActivitiesPartage();
	}
	
	@GET @Path("/endActivity/{idActivite}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public String endActivity(@PathParam("idActivite") int idActivite) {
		System.out.println("endActivity");
		return "" + dao.endActivity(idActivite);
	}
	
	@GET @Path("/endTask/{idTache}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public String endTask(@PathParam("idTache") int idTache) {
		System.out.println("endTask");
		return "" + dao.endTask(idTache);
	}
	
	@GET @Path("/partageActivite/{idActivite}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public String partageActivite(@PathParam("idActivite") int idActivite) {
		System.out.println("partageActivite");
		return "" + dao.partageActivite(idActivite);
	}
	
	@GET @Path("/pourcentageActiviteDone/{idCollaborateur}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public String pourcentageActiviteDone(@PathParam("idCollaborateur") int idCollaborateur) {
		System.out.println("pourcentageActiviteDone");
		return "" + dao.pourcentageActiviteDone(idCollaborateur);
	}
	
	@GET @Path("/createActivite/{idCollaborateur}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
	public String createActivite(@PathParam("idCollaborateur") int idCollaborateur) {
		System.out.println("createActivite");
		return "" + dao.createActivite("Dossier immo", "Finaliser dossier Dupont", "", idCollaborateur);
	}
}